#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//STRUCTURE SCORE
struct score{
	int attend;
	int assign;
	int quiz;
	int forum;
	int exam;
	struct score *scoreNext, *scorePrev;
};

//STRUCTURE STUDENT
struct student{
	char nim[20];
	char name[100];
	char course[100];
	struct student *studentNext, *studentPrev;
	struct score *scoreHead, *scoreTail, *scoreCurr;
}*studentHead, *studentTail, *studentCurr;

// DISPLAY ALL STUDENT DATA
void showStudentData(){
	
	int number 	= 1;
	studentCurr = studentHead;
	
	while(studentCurr){
		if(studentCurr->scoreHead == NULL) {
			printf("%d. %s | %s | %s \n", number++, studentCurr->nim, studentCurr->name, studentCurr->course);
		} else {
			printf("%d. %s | %s | %s * \n", number++, studentCurr->nim, studentCurr->name, studentCurr->course);
		}
		studentCurr = studentCurr->studentNext;
	}
}

// SHOW STUDENT DATA BY NIM
void detailStudentData(char nim[]){
	
	studentCurr = studentHead;
	while(studentCurr){
		if(strcmp(studentCurr->nim, nim) == 0){
			printf("\n## Detail Mahasiswa \n\n");
			printf("NIM            : %s\n", studentCurr->nim);
			printf("Nama Mahasiswa : %s\n", studentCurr->name);
			printf("Mata Kuliah    : %s\n\n", studentCurr->course);
			
			break;
		}
		studentCurr = studentCurr->studentNext;
	}
}

// SHOW STUDENT SCORE DATA BY NIM
void detailStudentScoreData(char nim[]){
	
	studentCurr = studentHead;
	while(studentCurr){
		if(strcmp(studentCurr->nim, nim) == 0){
			
			printf("\n## Detail Nilai Mahasiswa \n\n");
			printf("NIM            : %s\n", studentCurr->nim);
			printf("Nama Mahasiswa : %s\n", studentCurr->name);
			printf("Mata Kuliah    : %s\n\n", studentCurr->course);
			
			if(studentCurr->scoreHead == NULL) {
				printf("  - Nilai Absen : N/A \n");
				printf("  - Nilai Tugas : N/A \n");
				printf("  - Nilai Quis  : N/A \n");
				printf("  - Nilai Forum : N/A \n");
				printf("  - Nilai Ujian : N/A \n\n");
				
				printf("  Persentase per Nilai : \n");
				printf("  - Nilai Absen (10%%) : N/A \n");
				printf("  - Nilai Tugas (20%%) : N/A \n");
				printf("  - Nilai Quis  (10%%) : N/A \n");
				printf("  - Nilai Forum (10%%) : N/A\n");
				printf("  - Nilai Ujian (50%%) : N/A \n\n");
				printf("  - Total Nilai        : N/A \n");
				printf("  - Grade              : N/A \n");
				printf("  - Status             : N/A \n");
				
			} else {
				studentCurr->scoreCurr = studentCurr->scoreHead;
				
				calculateScore(
					studentCurr->scoreCurr->attend,
					studentCurr->scoreCurr->assign,
					studentCurr->scoreCurr->quiz,
					studentCurr->scoreCurr->forum,
					studentCurr->scoreCurr->exam
				);
			}
			
			break;
		}
		studentCurr = studentCurr->studentNext;
	}
}

// CALCULATE SCORE AND GET GRADE WITH STATUS
void calculateScore(int attend, int assign, int quiz, int forum, int exam) {
	float percentAttend, percentAssign, percentQuiz, percentForum, percentExam, totalScore;
	char grade[3], status[50];
	
	percentAttend 	= (float)(attend * 10) / 100;
	percentAssign 	= (float)(assign * 20) / 100;
	percentQuiz 	= (float)(quiz * 10) / 100;
	percentForum 	= (float)(forum * 10) / 100;
	percentExam 	= (float)(exam * 50) / 100;
	
	totalScore = percentAttend + percentAssign + percentQuiz + percentForum + percentExam;
	
	if(totalScore < 0 || totalScore > 100) {
		strcpy(grade, "N/A");
		strcpy(status, "Tidak Lulus");
	} else if(totalScore >= 50 && totalScore < 60) {
		strcpy(grade, "E");
		strcpy(status, "Tidak Lulus");
	} else if(totalScore >= 60 && totalScore < 70) {
		strcpy(grade, "D");
		strcpy(status, "Lulus");
	} else if(totalScore >= 70 && totalScore < 80) {
		strcpy(grade, "C");
		strcpy(status, "Lulus");
	} else if(totalScore >= 80 && totalScore < 90) {
		strcpy(grade, "B");
		strcpy(status, "Lulus");
	} else if(totalScore >= 90 && totalScore <= 100) {
		strcpy(grade, "A");
		strcpy(status, "Lulus");
	} else {
		strcpy(grade, "E");
		strcpy(status, "Tidak Lulus");
	}
	
	printf("  - Nilai Absen : %d\n", attend);
	printf("  - Nilai Tugas : %d\n", assign);
	printf("  - Nilai Quis  : %d\n", quiz);
	printf("  - Nilai Forum : %d\n", forum);
	printf("  - Nilai Ujian : %d\n\n", exam);
	
	printf("  Persentase per Nilai : \n");
	printf("  - Nilai Absen (10%%) : %.2f\n", percentAttend);
	printf("  - Nilai Tugas (20%%) : %.2f\n", percentAssign);
	printf("  - Nilai Quis  (10%%) : %.2f\n", percentQuiz);
	printf("  - Nilai Forum (10%%) : %.2f\n", percentForum);
	printf("  - Nilai Ujian (50%%) : %.2f\n\n", percentExam);
	printf("  - Total Nilai       : %.2f\n", totalScore);
	printf("  - Grade             : %s\n", grade);
	printf("  - Status            : %s\n", status);
}


// INSERT DATA STUDENT TO STRUCTURE
void addStudent(char nim[], char name[], char course[]){
	struct student *studentCurr = (struct student *)malloc(sizeof(struct student));
	
	strcpy(studentCurr->nim, nim);	
	strcpy(studentCurr->name, name);
	strcpy(studentCurr->course, course);
	
	studentCurr->studentNext 	= studentCurr->studentPrev = NULL;
	studentCurr->scoreHead 		= studentCurr->scoreTail = studentCurr->scoreCurr = NULL;
	
	if(studentHead==NULL){
		studentHead = studentTail = studentCurr;
	}else{
		studentTail->studentNext = studentCurr;
		studentCurr->studentPrev = studentTail;
		studentTail = studentCurr;
	}
	
	printf("\n**Data Mahasiswa Berhasil Disimpan!");
}

// INSERT DATA SCORE TO STRUCTURE
void addScore(char nim[], int attend, int assign, int quiz, int forum, int exam){
	int found		= -1;
	studentCurr 	= studentHead;
	
	while(studentCurr != NULL && found == -1){
		if(strcmp(studentCurr->nim, nim) == 0){ 
			found = 1; 
			break; 
		} 
		studentCurr = studentCurr->studentNext;
	}
	
	if(found==1) {
		studentCurr->scoreCurr = (struct score*)malloc(sizeof(struct score));
		studentCurr->scoreCurr->attend 	= attend;
		studentCurr->scoreCurr->assign	= assign;
		studentCurr->scoreCurr->quiz	= quiz;
		studentCurr->scoreCurr->forum	= forum;
		studentCurr->scoreCurr->exam	= exam;

		studentCurr->scoreCurr->scoreNext = studentCurr->scoreCurr->scorePrev = NULL;

		if(studentCurr->scoreHead==NULL){
			studentCurr->scoreHead = studentCurr->scoreTail = studentCurr->scoreCurr;
		}else{
			studentCurr->scoreTail->scoreNext = studentCurr->scoreCurr;
			studentCurr->scoreCurr->scorePrev = studentCurr->scoreTail;
			studentCurr->scoreTail = studentCurr->scoreCurr;
		}
		
		printf("\n**Data Nilai Berhasil Disimpan!");
	}else{
		printf("**NIM tidak terdaftar.\n");
	}
}

// VALIDATE STUDENT BY NIM
int validateStudent(char nim[]) {

	int found		= -1;
	studentCurr 	= studentHead;
	
	while(studentCurr != NULL && found == -1){
		if(strcmp(studentCurr->nim, nim) == 0){ 
			found = 1; 
			break; 
		} 
		studentCurr = studentCurr->studentNext;
	}
	
	if(found==1) {
		return 1;
	}else{
		return 0;
	}
}

// COUNT TOTAL STUDENT
int studentTotal() {
   int size = 0;

   if(studentHead != NULL) {
   		studentCurr = studentHead;
	   	while(studentCurr != NULL) {
	      studentCurr = studentCurr->studentNext;
	      size++;
	   	}
   }

   return size;
}


int main(){
	
	int validate 	= 1;
	char answer 	= 'Y';
	
	char nim[20], name[100], course[100];
	
	int selectedMenu, selectedNumber, scoreAttend, scoreAssign, scoreQuiz, scoreForum, scoreExam;
	
	while(answer == 'Y') {
		printf("====================================\n");
		printf("Program hitung nilai akhir mahasiswa\n");
		printf("====================================\n");
		
		printf("\n-------------\n");
		printf("Menu Navigasi");
		printf("\n-------------\n");
		printf("(1) Input Data Mahasiswa\n");
		printf("(2) Input Nilai\n");
		printf("(3) Lihat Nilai dan Status\n");
		printf("(4) Keluar");
		
		printf("\n\n");
		
		printf("Pilih Menu  : ");
		scanf(" %d", &selectedMenu);	
		
		switch(selectedMenu) {
			case 1:
				
				printf("\n");
				printf("## Input Data Mahasiswa ##");
				printf("\n");
		
				do {
					
					printf("\nNIM            : ");	
					scanf(" %s", nim);
						
					if(validateStudent(nim) == 1) {
						validate = 1;
						printf("**NIM sudah terdaftar");
					} else {
						validate = 0;
					}
					
				} while(validate == 1);
				
				printf("Nama Mahasiswa : ");
				scanf(" %[^\n]s", name);
				printf("Mata Kuliah    : ");
				scanf(" %[^\n]s", course);
				
				addStudent(nim, name, course);
				
				break;
				
			case 2:
				
				if(studentTotal() == 0) {
					printf("\n------------------------------\n");
					printf("        Perhatian!          \n");
					printf("Data Mahasiswa tidak ditemukan\n");
					printf("Silakan input terlebih dahulu.\n");
					printf("------------------------------\n");
				} else {
					printf("\n");
					printf("## Input Nilai Mahasiswa ##");
					printf("\n");
					
					printf("\n--------------\n");
					printf("Data Mahasiswa");
					printf("\n--------------\n");
					
					showStudentData();
					
					printf("\n");
					printf("Total Mahasiswa : %d", studentTotal());
					
					do {
						
						printf("\n\nMasukkan NIM  : ");
						scanf(" %s", nim);
							
						if(validateStudent(nim) == 0) {
							validate = 1;
							printf("NIM tidak terdaftar");
						} else {
							validate = 0;
							detailStudentData(nim);
						}
						
					} while(validate == 1);
					
					printf("## Masukkan Nilai \n\n");
					printf("Nilai Hadir  : ");
					scanf(" %d", &scoreAttend);
					printf("Nilai Tugas : ");
					scanf(" %d", &scoreAssign);
					printf("Nilai Quiz : ");
					scanf(" %d", &scoreQuiz);
					printf("Nilai Keaktifan Forum : ");
					scanf(" %d", &scoreForum);
					printf("Nilai UAS : ");
					scanf(" %d", &scoreExam);
					
					addScore(nim, scoreAttend, scoreAssign, scoreQuiz, scoreForum, scoreExam);
					
				}
				
				break;
				
			case 3:
				
				if(studentTotal() == 0) {
					printf("\n------------------------------\n");
					printf("        Perhatian!          \n");
					printf("Data Mahasiswa tidak ditemukan\n");
					printf("Silakan input terlebih dahulu.\n");
					printf("------------------------------\n");
				} else {
					printf("\n------------------------------------\n");
					printf("Data Mahasiswa");
					printf("\n------------------------------------\n");
					
					showStudentData();
					
					printf("\n");
					printf("Total Mahasiswa : %d", studentTotal());
					
					do {
						
						printf("\n\nMasukkan NIM  : ");
						scanf(" %s", nim);
							
						if(validateStudent(nim) == 0) {
							validate = 1;
							printf("**NIM tidak terdaftar");
						} else {
							validate = 0;
						}
						
					} while(validate == 1);
					
					detailStudentScoreData(nim);
				}
				break;
				
			case 4:
				
				system("cls");
				
				printf("\n----------------------------------\n");
				printf("      **Selamat Tinggal**       \n");
				printf("Kamu berhasil keluar dari aplikasi\n");
				printf("------------------------------------\n");
				
				exit(0);
				break;
		}
		
		printf("\n \nKembali ke Menu? (Y/N) : ");
    	scanf(" %c", &answer);
		
		system("cls");
	}
}
